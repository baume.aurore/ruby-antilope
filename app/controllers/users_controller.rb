class UsersController < ApplicationController
  before_action :load_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all.limit(20).order(:last_name)
    @count = User.count
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new user_attributes
    if @user.save
      redirect_to user
    else
      render :new
    end
  end

  def update
    if @user.update user_attributes
      redirect_to @user
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to users_path
  end

  def load_user
    @user = User.find(params[:id])
  end

  def user_attributes
    attributes = params[:user].permit(:first_name, :last_name, :birthdate)
  end

end
