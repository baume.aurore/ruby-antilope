class PhotosController < ApplicationController
  before_action :load_user
  before_action :load_photo, only: [:show, :edit, :update, :destroy]

  def index
    @photos = @user.photos
    @count = @photos.count
  end

  def new
    @photo = Photo.new
  end

  def create
    @photo = @user.photos.build photo_attributes
    if @photo.save
      redirect_to [@user, @photo]
    else
      render :new
    end
  end

  def show
  end

  def update
    if @photo.update photo_attributes
      redirect_to [@user, @photo]
    else
      render :edit
    end
  end

  def destroy
    @photo.destroy
    redirect_to @user
  end

  def load_user
    @user = User.find(params[:user_id])
  end

  def load_photo
    @photo = Photo.find(params[:id])
  end

  def photo_attributes
    attributes = params[:photo].permit(:name, :image)
  end
end
