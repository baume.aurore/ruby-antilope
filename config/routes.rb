Rails.application.routes.draw do
  devise_for :users
  resources :users do
    resources :photos
  end

  root to: 'users#index'
end
